package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		if(loginName != null) {
			
			return loginName.matches("/[0-9a-zA-Z]{6,}/");
		}
		return false;
	}
}
