package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginNameRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Changyiq12" ) );
	}
	
	@Test
	public void testIsValidLoginNameException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "!@#$%^" ) );
	}
	
	@Test
	public void testIsValidLoginNameBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Changyiq!!" ) );
	}
	
	@Test
	public void testIsValidLoginNameBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Changyiq12" ) );
	}

	@Test
	public void testIsValidLoginNameLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Changyiq12" ) );
	}
	
	@Test
	public void testIsValidLoginNameLengthException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( " " ) );
	}
	
	@Test
	public void testIsValidLoginNameLengthBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Chang" ) );
	}
	
	@Test
	public void testIsValidLoginNameLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Chang1" ) );
	}
}
